import React, { useState } from 'react';
import { StyleSheet, Alert } from 'react-native';

export default function FlatListColor(){
    const [backgroundColor, setBackgroundColor] = useState("rgba(255,255,213,.9)");
    return(
      <>
      <ColorForm onNewColor={newColor=>Alert.alert(`add color: ${newColor}`)}/>
      <FlatList style={[styles.flatList,{backgroundColor}]}
        data={defaultColors}
        renderItem={({item})=>{
          return(
            <ColorButton key={item.id} backgroundColor={item.color} onPress={setBackgroundColor}/>
          );
        }}
      /> 
      </>     
    );
}

const styles = StyleSheet.create({
    flatList:{
      flex: 1,
      display:"flex",    
    },
  });
